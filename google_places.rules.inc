<?php

/**
 * Implement hook_rules_action_info()
 * Declare any meta data about actions for Rules
 */
function google_places_rules_action_info()
{
  $actions = array(
    'google_places_query' => array(
      'label' => t('Query Google Places API to find a place'),
      'group' => t('Google Places'),
      'parameter' => array(
        'place' => array(
          'type' => 'node',
          'bundle' => 'google_place',
          'label' => t('Google Place')
        )
      ),
    ),
    'google_places_get_details' => array(
      'label' => t('Load place details from Google Places API'),
      'group' => t('Google Places'),
      'parameter' => array(
        'place' => array(
          'type' => 'node',
          'bundle' => 'google_place',
          'label' => t('Google Place')
        )
      ),
    ),
  );

  return $actions;
}

/**
 * The action function for rules_example_action_hello_world
 */
function google_places_query($place)
{
  $wrapper = entity_metadata_wrapper('node', $place);
  if ($wrapper->field_reference->value() != "") {
//    watchdog('google_places', "Disregarding Google Places API query for place with reference");
    return;
  }

  if ($wrapper->field_locked->value()) {
//    watchdog('google_places', t('Skipping locked place: ' . $wrapper->title->value()));
    return;
  }

//  watchdog('google_places', 'Query Google Places API for ' . $wrapper->title->value());

  $location = geocoder('google', $wrapper->field_zipcode->value());

  $params = array(
    'query' => $wrapper->title->value(),
    'radius' => 50000
  );

  if (property_exists($location, 'coords')) {
    $params['location'] = $location->coords[1] . "," . $location->coords[0];
  }


  try {
    $wrapper->field_query->set(json_encode($params));
  } catch (Exception $e) {
    watchdog('google_places', print_r($e, true));
  }


  $data = _google_places_api_request('textsearch', $params);

//  watchdog('google_places', 'results: ' . print_r($data, true));

  if ($data == null) {
    watchdog('google_places', "Unable to update Google Place " . $wrapper->title->value());
    return null;
  }
  $results = $data->results;

  if (count($results) > 0) {
    $first_result = $results[0];

    try {
      if ($first_result->name) {
        $wrapper->field_place_name->set($first_result->name);
      }
      if ($first_result->reference) {
        $wrapper->field_reference->set($first_result->reference);
      }
      if ($first_result->formatted_address) {
        $wrapper->field_address->set($first_result->formatted_address);
      }

      if ($first_result->id) {
        $wrapper->field_google_id->set($first_result->id);
      }

      if ($first_result->geometry) {

        $loc = array(
          'lat' => $first_result->geometry->location->lat,
          'lon' => $first_result->geometry->location->lng,
        );

        $wrapper->field_location->set($loc);
      }

      $arr = array();
      foreach ($results as $r) {
        array_push($arr, json_encode($r));
      }

      $wrapper->field_json->set($arr);
    } catch (Exception $e) {
      watchdog('google_places', print_r($e, true));
    }
  }

  $wrapper->save();
}


/**
 * The action function for google_places_get_details
 */
function google_places_get_details($place)
{
  $wrapper = entity_metadata_wrapper('node', $place);

  if ($wrapper->field_locked->value()) {
//    watchdog('google_places', 'Skipping locked place: ' . $wrapper->title->value());
    return;
  }

//  watchdog('google_places', 'Request Google Places API details for ' . $wrapper->title->value());

  $data = _google_places_api_request('details', array('reference' => $wrapper->field_reference->value()));

//  watchdog('google_places', 'details: ' . print_r($data, true));

  if ($data && property_exists($data, 'result')) {

    try {
      if (property_exists($data->result, 'formatted_phone_number'))
        $wrapper->field_phone_number->set($data->result->formatted_phone_number);

      if (property_exists($data->result, 'website'))
        $wrapper->field_website->set($data->result->website);

      $wrapper->save();

      if (property_exists($data->result, 'photos')) {
//        watchdog('google_places', 'will load photos '.print_r($data->result->photos, true));
        _google_places_load_photos($place, $data->result->photos);
      }


    } catch (Exception $e) {
      watchdog('google_places', print_r($e, true));
    }
  }
}


/**
 * Load google photos for place
 */
function _google_places_load_photos($place, $photos) {

  $wrapper = entity_metadata_wrapper('node', $place);

  $collections = array();
  foreach ($photos as $photo) {

    $image_data = _google_places_retrieve_photo($photo);

    if (property_exists($image_data, 'data')) {
      $file = file_save_data($image_data->data);

      $fc = entity_create('field_collection_item', array('field_name' => 'field_selected_photos', 'revision_id' => 1));
      $fc->setHostEntity('node', $place);

      $fc_wrapper = entity_metadata_wrapper('field_collection_item', $fc);

      $fc_wrapper->field_google_photo->set((array)$file);

      $fc->save(TRUE);
      $collections[] = $fc;
    }
  }

  $wrapper->field_selected_photos->set($collections);
  $wrapper->save();

}


/**
 * Retrieve image from google places photos
 */
function _google_places_retrieve_photo($photo) {
  $photo = (array)$photo;
  $params = array(
    'photoreference' => $photo['photo_reference'],
    'maxwidth' => 1024
  );
  return _google_places_api_request('photo', $params);
}



/**
 * Hit the Google Places API
 *
 * @param $method
 * @param $params
 * @return object https://api.drupal.org/api/drupal/includes%21common.inc/function/drupal_http_request/7
 */
function _google_places_api_request($method, $params)
{

  $params['sensor'] = 'false';
  $params['key'] = variable_get("google_places_api_key");

  $querystring = http_build_query($params);

  $url = 'https://maps.googleapis.com/maps/api/place/' . $method;
  if ($method != 'photo') {
    $url .= '/json';
  }
  $url .= '?' . $querystring;

  $response = drupal_http_request($url);

  if ($method == 'photo') {
    return $response;
  }

  if (property_exists($response, 'data')) {
    $data = json_decode($response->data);

    if (is_object($data) && $data->status == 'OVER_QUERY_LIMIT') {
      watchdog('google_places', 'OVER_QUERY_LIMIT');
      return null;
    }

    return $data;
  }

  return null;
}
