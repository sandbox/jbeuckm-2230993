This module requires a patch for field_collection 7.x-1.0-beta7

wget https://drupal.org/files/1937866-field_collection-metadata-setter.patch
patch -p1 < 1937866-field_collection-metadata-setter.patch