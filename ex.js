var t = {"html_attributions":[], "result":{"address_components":[
    {"long_name":"21", "short_name":"21", "types":["subpremise"]},
    {"long_name":"2395", "short_name":"2395", "types":["street_number"]},
    {"long_name":"York Rd", "short_name":"York Rd", "types":["route"]},
    {"long_name":"Jamison", "short_name":"Jamison", "types":["locality", "political"]},
    {"long_name":"PA", "short_name":"PA", "types":["administrative_area_level_1", "political"]},
    {"long_name":"United States", "short_name":"US", "types":["country", "political"]},
    {"long_name":"18929", "short_name":"18929", "types":["postal_code"]},
    {"long_name":"1071", "short_name":"1071", "types":[]}
], "adr_address":"<span class=\"street-address\">2395 York Rd #21<\/span>, <span class=\"locality\">Jamison<\/span>, <span class=\"region\">PA<\/span> <span class=\"postal-code\">18929<\/span>, <span class=\"country-name\">United States<\/span>", "formatted_address":"2395 York Rd #21, Jamison, PA, United States", "formatted_phone_number":"(215) 491-5720", "geometry":{"location":{"lat":40.260112, "lng":-75.086172}}, "icon":"http:\/\/maps.gstatic.com\/mapfiles\/place_api\/icons\/restaurant-71.png", "id":"3e8845f763c0eb87ba0f482910280df6952adb82", "international_phone_number":"+1 215-491-5720", "name":"McDonald's", "opening_hours":{"open_now":true, "periods":[
    {"close":{"day":0, "time":"2300"}, "open":{"day":0, "time":"0500"}},
    {"close":{"day":1, "time":"2300"}, "open":{"day":1, "time":"0500"}},
    {"close":{"day":2, "time":"2300"}, "open":{"day":2, "time":"0500"}},
    {"close":{"day":3, "time":"2300"}, "open":{"day":3, "time":"0500"}},
    {"close":{"day":4, "time":"2300"}, "open":{"day":4, "time":"0500"}},
    {"close":{"day":6, "time":"0000"}, "open":{"day":5, "time":"0500"}},
    {"close":{"day":0, "time":"0000"}, "open":{"day":6, "time":"0500"}}
]}, "price_level":1, "reference":"CnRtAAAA7AFoAQQl8Z1jlt8RrSu28OhpKVIMbFl3S_BaitfbNy_W91I4nO8Xbhdd-e5tn_nGdBpk9aLXjzBWUs33HaHGuYaf0D5VQrb5raua_jOq5g7TPq7ozojEaxx0ckPn-LYqMPXB9zpyLOVuJq7z2O_IEhIQsl7n0Ky68lWaK8ZVDKuT5hoUAhh4uq5auLxdPe1BLTPWCU0Pyh8", "types":["restaurant", "food", "establishment"], "url":"https:\/\/plus.google.com\/103446295598192386771\/about?hl=en-US", "utc_offset":-240, "vicinity":"2395 York Rd #21, Jamison", "website":"http:\/\/www.mcdonalds.com\/"}, "status":"OK"}
